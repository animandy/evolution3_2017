#include <iostream> 
using namespace std; 
int main() { 
    char str[20], * ptr; 
    int number; 
    ptr = str; 
    cout << "Input: "; 
    while((*ptr++ = getchar()) != '\n'); 
    *--ptr = '\0'; 
    number = atoi(str); 
    if (number || (str[0] == '0' && str[1] == '\0')) cout << "Output: " << number << endl; 
    else cout << "\a Error!" << endl; 
    cin.get(); 
    return 0; 
}