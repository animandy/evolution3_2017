package seabattle.model.events;

import java.util.EventListener;

/**
 * Слушатель изменения состояния игры
 */
public interface GameListener extends EventListener {
    public void gameFinished(GameEvent e);
    public void moveIsDone(GameEvent e);
    public boolean askPanelToGetAttributes(GameEvent e);
}
